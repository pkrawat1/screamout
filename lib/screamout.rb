require "screamout/engine"

module Screamout
  require "bson"
  require 'mongoid-rspec'
  require 'simple_form'
  require 'twitter-typeahead-rails'
  require 'jquery-ui-rails'
  require 'imgkit'
  require 'jquery-rails'
  require 'mongoid_taggable'
  require 'mongoid_rateable'
  require 'sidekiq'
  require 'haml-rails'
  require 'mongoid_paperclip'

end
